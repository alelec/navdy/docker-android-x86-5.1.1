-------------------
docker-android-x86-5.1.1
------------------------

Docker image extended from butomo1989/docker-android-x86-5.1.1 with tools for running Navdy-MapDownloader

This image can be used to build android apps and run them in the internal emulator.