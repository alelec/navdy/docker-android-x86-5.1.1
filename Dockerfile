FROM budtmo/docker-android-x86-5.1.1:1.8-p14
MAINTAINER https://gitlab.com/alelec/navdy/docker-android-x86-5.1.1

# RUN apt update && \
#     apt install -y software-properties-common python-software-properties && \
#     add-apt-repository -y ppa:alessandro-strada/ppa && apt-get update && \
#     apt install -y google-drive-ocamlfuse

#RUN curl https://sh.rustup.rs -sSf | bash -s -- -y

RUN apt update || true; \
    apt install -y libfuse-dev pkg-config libssl-dev sshfs locales nano screen

# Locale setup
RUN locale-gen en_US.UTF-8

ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8

RUN dpkg-reconfigure locales

ENV LC_ALL=en_US.UTF-8

# Google Drive client
#RUN . $HOME/.cargo/env; cargo install gcsf
#ENV PATH="${PATH}:/root/.cargo/bin"
